<?php
	// 1. Create a database connection
	define("DB_SERVER", "localhost");
	define("DB_USER", "ideapad_cms");
	define("DB_PASS", "secretpassword");
	define("DB_NAME", "ideapad");
	/*
	$dbhost = "localhost";
	$dbuser = "ideapad_cms";
	$dbpass = "secretpassword";
	$dbname = "ideapad";
	$db = mysqli_connect($dbhost, $dbuser, $dbpass, $dbname);
	*/
	$db = mysqli_connect(DB_SERVER, DB_USER, DB_PASS, DB_NAME);
	// Test if connection succeeded
	if (mysqli_connect_errno()) {
		die("Database connection failed: " . 
			mysqli_connect_error() . 
			" (" . mysqli_connect_errno() . ")"
		);
	}
?>