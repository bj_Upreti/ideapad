<?php 
	/*
	 *create_subject.php is going to do the form processing.
	 *It won't render any HTML and at the end it will redirect the user to an appropriate HTML page.
	 *On success it will redirect to one page and on failure it will redirect to another page.
	 */

?>
<?php require_once("../includes/session.php"); ?>
<?php require_once("../includes/db_connection.php"); ?>
<?php require_once("../includes/functions.php"); ?>
<?php require_once("../includes/validation_functions.php"); ?>
<?php
	if (isset($_POST['submit'])) {
		// validations
		$required_fields = ["menu_name", "position", "visible"];
		validate_presences($required_fields);

		$fields_with_max_lengths = ["menu_name" => 30];
		validate_max_lengths($fields_with_max_lengths);
		if (!empty($errors)) {
			$_SESSION["errors"] = $errors;
			redirect_to("new_subject.php");
		} // if the errors[] is not empty
		else {
			// Process the form
			$menu_name = mysql_prep($_POST['menu_name']);
			$position = (int) $_POST['position']; // Typecasting into an integer
			$visible = (bool) $_POST['visible']; // Typecasting into a boolean
			$query = "INSERT INTO subjects (";
			$query .= " menu_name, position, visible";
			$query .= ") VALUES (";
			$query .= " '{$menu_name}', {$position}, {$visible}";
			$query .= ")";
			$result = mysqli_query($db, $query);
			if ($result) {
				// Success
				$_SESSION["message"] = "Subject created";
				redirect_to("manage_content.php");
			}
			else {
				// Failure
				$_SESSION["message"] = "Subject creation failed.";
				redirect_to("new_subject.php");
			}
		} // if the errors[] is empty
	} // if it is a post request
	else {
		// This is probably a get request.
		redirect_to("new_subject.php");
	}
?>

<?php require_once("../includes/functions.php"); ?>
<?php 
	if(isset($db)) { mysqli_close($db); }  
	?>