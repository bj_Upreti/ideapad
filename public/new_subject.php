<?php require_once("../includes/session.php"); ?>
<?php require_once("../includes/db_connection.php"); ?>
<?php require_once("../includes/functions.php"); ?>
<?php include("../includes/layouts/header.php"); ?>

<?php find_selected_page(); ?>
<div id="main">
	<div id="navigation">
		<?php echo navigation($current_subject, $current_page); ?>
	</div>
	<div id="page">
		<?php echo message(); ?>
		<?php $errors = errors(); ?>
		<?php echo form_errors($errors); ?>
		<h2>Create Subject</h2>
		<form action="create_subject.php" method="POST">
			<p>Menu name: 
				<input type="text" name="menu_name" value="" />
			</p>
			<p>Position: 
				<select name="position">
				<?php
					/* Efficient way of count using count() function in mysql
					$query = "SELECT";
					$query .= " COUNT(*) AS count";
					$query .= " FROM subjects";
					$subject_count = mysqli_fetch_assoc(mysqli_query($db, $query));
					for($count=1; $count <= $subject_count['count'] + 1; $count++) { */
					$subject_set = find_all_subjects();
					$subject_count = mysqli_num_rows($subject_set);
					for($count=1; $count <= $subject_count + 1; $count++) {
						echo "<option value=\"{$count}\">{$count}</option>";
					}
				?>
				</select>
			<p>Visible: 
				<input type="radio" name="visible" value="0" /> No
				&nbsp;
				<input type="radio" name="visible" value="1" /> Yes
			</p>
			<input type="submit" name="submit" value="Create Subject">
		</form>
		<br />
		<a href="manage_content.php">Cancel</a>
	</div><!-- #page ends here -->
</div><!-- #main ends here -->
<?php include("../includes/layouts/footer.php"); ?>