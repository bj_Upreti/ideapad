<?php require_once("../includes/session.php"); ?>
<?php require_once("../includes/db_connection.php"); ?>
<!-- function can't be define more than once so we want to ensure that it is included only once that's why require_once is better approach then require or include -->
<?php require_once("../includes/functions.php"); ?>
<?php include("../includes/layouts/header.php"); ?>

<?php find_selected_page(); ?>
<div id="main">
	<div id="navigation">
		<?php echo navigation($current_subject, $current_page); ?>
		<br />
		<a href="new_subject.php">+ Add a subject</a>
	</div>
	<div id="page">
		<?php echo message(); ?>
		<?php if (isset($current_subject)) { ?>
			<h2>Manage Subject</h2>
			<?php echo "Menu name: " . $current_subject["menu_name"] . "<br />"; ?>
			<a href="edit_subject.php?subject=<?php echo $current_subject["id"]; ?>">Edit Subject</a>
		<?php } elseif (isset($current_page)) { ?>
			<h2>Manage Page</h2>
			<?php echo "Menu name: " . $current_page["menu_name"] . "<br />"; ?>
			<a href="edit_page.php?page=<?php echo $current_page["id"]; ?>">Edit Page</a>
		<?php } else { ?>
			Please select a subject or a page.
		<?php } ?>
	</div><!-- #page ends here -->
</div><!-- #main ends here -->
<?php include("../includes/layouts/footer.php"); ?>